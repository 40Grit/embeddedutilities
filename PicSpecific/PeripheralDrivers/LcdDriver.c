#include <pic.h>

#define ICD_DEBUG

void lcdDriverInit(void)
{
    //Select Frame Clock prescale using bits LP<3:0> of LCDPS

	ANSELE = 0x11;

	LCDPSbits.WFT = 0b0;		//Type A waveform
	LCDPSbits.BIASMD = 0b0;		//1/3 Bias
	LCDPSbits.LP = 0b0110;		// Prescaler


	//Configure appropriate pins to function as segments using LCDSEn registers
    LCDSE0 = 0xFF;  //<7:0>
    LCDSE1 = 0x38;	//<15:8>
    LCDSE2 = 0xFB;	//<23:16>
    LCDSE3 = 0xB6;  //<24:31>
    LCDSE4 = 0x30;	//<39:32>
    LCDSE5 = 0x3A;	//<45:40>

#ifdef ICD_DEBUG //Segment cap
	LCDSE4bits.SE38 = 0;
	LCDSE4bits.SE39 = 0;
#endif

    //Configure LCD module using LCDCON reg
        //multiplex and bias mode
        LCDCONbits.LMUX = 0b11;
        //Timing source
        LCDCONbits.CS	= 0b01;
        //Sleep mode
        LCDCONbits.SLPEN = 0;

    //Write initial values to pixel data registers, LCDDATA0 through LCDDATA23
    LCDDATA0 = 0x00;
    LCDDATA1 = 0x00;
    LCDDATA2 = 0x00;
    LCDDATA3 = 0x00;
    LCDDATA4 = 0x00;
    LCDDATA5 = 0x00;
    LCDDATA6 = 0x00;
    LCDDATA7 = 0x00;
    LCDDATA8 = 0x00;
    LCDDATA9 = 0x00;
    LCDDATA10 = 0x00;
    LCDDATA11 = 0x00;
    LCDDATA12 = 0x00;
    LCDDATA13 = 0x00;
    LCDDATA14 = 0x00;
    LCDDATA15 = 0x00;
    LCDDATA16 = 0x00;
    LCDDATA17 = 0x00;
    LCDDATA18 = 0x00;
    LCDDATA19 = 0x00;
    LCDDATA20 = 0x00;
    LCDDATA21 = 0x00;
    LCDDATA22 = 0x00;
    LCDDATA23 = 0x00;


    //Clear LCD interrupt flag, if desired enable interrupt
    LCDIF = 0;

    //Configure bias voltages by setting the LCDRL, LCDREF, and ANSELx registers as needed


    // Reference ladder control
    LCDREFbits.LCDIRE	= 0b1; // Internal reference enabled
	LCDREFbits.LCDIRS	= 0b0; // Internal Reference Source
	LCDREFbits.LCDIRI	= 0b0; // Internal Reference always on
	LCDREFbits.VLCD1PE	= 0b0;
	LCDREFbits.VLCD2PE	= 0b0;
	LCDREFbits.VLCD3PE	= 0b0;
	
			
	LCDRLbits.LRLAP = 0b11;
	LCDRLbits.LRLBP = 0b11;
	LCDRLbits.LRLAT = 0b000;

    //Set Contrast
    LCDCST = 0b000;     // maximum contrast

    //Enable the LCD module by setting LCDEN of LCDCON
    LCDEN = 1;
}


void lcdDriverOff(void)
{
    LCDEN = 0;
}

void lcdDriverOn(void)
{
    LCDEN = 1;
}

void lcdDriverClearSegments(void)
{
	LCDDATA0 = 0x00;
    LCDDATA1 = 0x00;
    LCDDATA2 = 0x00;
    LCDDATA3 = 0x00;
    LCDDATA4 = 0x00;
    LCDDATA5 = 0x00;
    LCDDATA6 = 0x00;
    LCDDATA7 = 0x00;
    LCDDATA8 = 0x00;
    LCDDATA9 = 0x00;
    LCDDATA10 = 0x00;
    LCDDATA11 = 0x00;
    LCDDATA12 = 0x00;
    LCDDATA13 = 0x00;
    LCDDATA14 = 0x00;
    LCDDATA15 = 0x00;
    LCDDATA16 = 0x00;
    LCDDATA17 = 0x00;
    LCDDATA18 = 0x00;
    LCDDATA19 = 0x00;
    LCDDATA20 = 0x00;
    LCDDATA21 = 0x00;
    LCDDATA22 = 0x00;
    LCDDATA23 = 0x00;
}

//void lcdDriverSetContrast(uint8_t contrast)
//{
//    if(contrast > 7) return;
//
//    contrast = ~contrast;
//    LCDCST = contrast;
//}