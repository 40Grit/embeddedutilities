#ifndef PWM_H
#define PWM_H

#include <pic.h>
#include <stdlib.h>
#include <stdint.h>

void pwmChangeDuty(void);
void pwmInit(void);

int8_t pwmRequestEnable(void);
int8_t pwmRequestDisable(void);

uint24_t pwmPercentage;

#endif /*PWM_H*/

