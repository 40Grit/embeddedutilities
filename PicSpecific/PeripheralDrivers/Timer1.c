#include <pic.h>
#include <stdint.h>
#include <stdlib.h>

void timer1Init(void)
{
    T1CONbits.TMR1CS0 = 0;
    T1CONbits.TMR1CS1 = 1;

    T1CONbits.T1OSCEN = 1;

	//FIXME: Implement Stabilization
    TMR1H = 0x80;
    TMR1L = 0x00;
    T1CONbits.nT1SYNC = 1;
	T1CONbits.T1CKPS0 = 0;
	T1CONbits.T1CKPS1 = 0;
    T1CONbits.TMR1ON = 1;
	PIR1bits.TMR1IF = 0;
    PIE1bits.TMR1IE = 1;
}

void timer1Reset(void)
{
    T1CONbits.TMR1ON = 0;
    TMR1H = 0x80;
    TMR1L = 0x00;
    T1CONbits.TMR1ON = 1;
}

uint8_t timer1IsEnabled(void)
{
	return T1CONbits.TMR1ON;
}