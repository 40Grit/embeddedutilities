#ifndef IOPORTS_H
#define IOPORTS_H

#include <stdlib.h>
#include <stdint.h>

uint8_t readRB0(void);
uint8_t readRB1(void);
uint8_t readRB2(void);
uint8_t readRB3(void);

#endif /*IOPORTS_H*/