//PIC16f1947 Timer 2 Driver
#include <stdint.h>
#include <stdlib.h>
#include <pic.h>

static uint8_t subscribers = 0;

int8_t timer2Init(uint8_t setPoint, uint8_t prescaler, uint8_t postscaler)
{
    PIE1bits.TMR2IE = 0;
    PIR1bits.TMR2IF = 0;
    T2CONbits.TMR2ON = 0;

    switch (prescaler)
    {
        case 1:
            T2CONbits.T2CKPS = 0b00;
            break;
        case 4:
            T2CONbits.T2CKPS = 0b01;
            break;
        case 16:
            T2CONbits.T2CKPS = 0b10;
            break;
        case 64:
            T2CONbits.T2CKPS = 0b11;
            break;
        default:
            return -1;
    }

    if ((postscaler == 0) || (postscaler > 16))
    {
        return -1;
    }
    else
    {
        T2CONbits.T2OUTPS = (uint8_t)(postscaler - 1);
    }

    PR2 = setPoint;
    subscribers = 0;
    return 0;
}


int8_t timer2RequestEnable(uint8_t enable)
{
    if(enable) 
		if(subscribers++)	return 1;

	if(!enable) 
		if(subscribers--)	return 1;


    PIR1bits.TMR2IF		= 0;
    T2CONbits.TMR2ON	= enable;
	PIE1bits.TMR2IE		= enable;

    return 0;
}

//int8_t timer2RequestDisable(void)
//{
//    if(--subscribers)
//        return 1;
//
//    PIE1bits.TMR2IE = 0;
//    T2CONbits.TMR2ON = 0;
//
//    return 0;
//}


int8_t timer2IsEnabled(void)
{
	return T2CONbits.TMR2ON;
}
