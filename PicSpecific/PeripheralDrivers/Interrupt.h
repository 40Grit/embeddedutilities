/* 
 * File:   Interrupt.h
 * Author: Grit
 *
 * Created on February 12, 2014, 6:47 PM
 */

#ifndef INTERRUPT_H
#define	INTERRUPT_H

void interruptGlobalEnable(uint8_t enable);
void interruptPeripheralEnable(uint8_t enable);

void interruptOnChangeInit(void);


void interruptOnChangeEnable(uint8_t enable);

#endif	/* INTERRUPT_H */

