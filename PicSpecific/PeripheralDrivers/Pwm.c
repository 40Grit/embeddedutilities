#include <pic.h>
#include <stdint.h>
#include <stdlib.h>
#include "Pwm.h"

static uint8_t subscribers = 0;

void pwmChangeDuty(void)
{
	uint24_t CCPR3DutyValue;
	uint24_t pr4Holder;
	pr4Holder = PR4;

	CCPR3DutyValue = pwmPercentage * (pr4Holder + 1);
	CCPR3DutyValue /= 25;

    CCPR3L = CCPR3DutyValue>>2;
}


void pwmInit(void)
{
	//Disable the CCP3 pin output driver
	TRISGbits.TRISG0 = 1;

	//Select timer 4 as CCP3 PWM resource
	CCPTMRS0bits.C3TSEL = 0b01;

	//Load PR4 with PWM period
	PR4 = 0x50;

	//Configure CCP3CON for PWM
	CCP3CONbits.CCP3M0 = 0;
	CCP3CONbits.CCP3M1 = 0;
	CCP3CONbits.CCP3M2 = 1;
	CCP3CONbits.CCP3M3 = 1;

	CCP3CONbits.P3M0 = 0;
	CCP3CONbits.P3M1 = 0;

	//Load CCPR3L Register & DC3B4 bits with duty cycle
	CCPR3L = 0b10000000;
	CCP3CONbits.DC3B1 = 0;
	CCP3CONbits.DC3B0 = 0;

	//Configure Timer 4 for PWM
	TMR4IE = 0;
	TMR4IF = 0;
	T4CONbits.T4CKPS = 0b00;
	TMR4ON = 0;
	pwmPercentage = 50;
	pwmChangeDuty();
    subscribers = 0;
}

int8_t pwmRequestEnable(void)
{
    if(subscribers++)
        return 1;
    
    TMR4ON = 1;
    TRISGbits.TRISG0 = 0;
    return 0;
}


int8_t pwmRequestDisable(void)
{
    if(--subscribers)
        return 1;

    TRISGbits.TRISG0 = 1;
    TMR4ON = 0;
    return 0;
}

