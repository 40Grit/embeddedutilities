//PIC16f1947 Timer 4 Driver
#include <stdint.h>
#include <stdlib.h>
#include <pic.h>

static uint8_t subscribers = 0;

int8_t timer4Init(uint8_t setPoint, uint8_t prescaler, uint8_t postscaler)
{
    PIE3bits.TMR4IE = 0;
    PIR3bits.TMR4IF = 0;
    T4CONbits.TMR4ON = 0;

    switch (prescaler)
    {
        case 1:
            T4CONbits.T4CKPS = 0b00;
            break;
        case 4:
            T4CONbits.T4CKPS = 0b01;
            break;
        case 16:
            T4CONbits.T4CKPS = 0b10;
            break;
        case 64:
            T4CONbits.T4CKPS = 0b11;
            break;
        default:
            return -1;
    }

    if ((postscaler == 0) || (postscaler > 16))
    {
        return -1;
    }
    else
    {
        T4CONbits.T4OUTPS = (uint8_t)(postscaler - 1);
    }

    PR4 = setPoint;
    subscribers = 0;
    return 0;
}

int8_t timer4RequestEnable(void)
{
    if(subscribers++)
       return 1;

    TMR4IF = 0;
    TMR4ON = 1;
    TMR4IE = 1;
    return 0;
}

int8_t timer4RequestDisable(void)
{
    if(--subscribers)
        return 1;

    TMR4IE = 0;
    TMR4ON = 0;
    
    return 0;
}

int8_t timer4IsEnabled(void)
{
	return T4CONbits.TMR4ON;
}
