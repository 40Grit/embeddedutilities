#include <pic.h>
#include <stdint.h>
#include <stdlib.h>

void interruptGlobalEnable(uint8_t enable)
{
	INTCONbits.GIE = enable;
}

void interruptPeripheralEnable(uint8_t enable)
{
	INTCONbits.PEIE = enable;
}

void interruptOnChangeInit(void)
{
	PORTB &= 0b11110000; //Clear PORTB(0:3)
	TRISB |= 0b00001111; //PORTB (0:3) as inputs
    IOCBN =  0b00001111;

    INTCONbits.IOCIE = 1;
}

void interruptOnChangeEnable(uint8_t enable)
{
    INTCONbits.IOCIE = enable;
}
