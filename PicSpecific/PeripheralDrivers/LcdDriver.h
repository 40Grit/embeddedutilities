#ifndef LCDDRIVER_H
#define LCDDRIVER_H

void lcdDriverInit(void);
void lcdDriverOff(void);
void lcdDriverOn(void);
void lcdDriverClearSegments(void);
void lcdDriverSetContrast(uint8_t contrast);

#endif /*LCDDRIVER_H*/