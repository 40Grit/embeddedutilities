#include <pic.h>
#include <stdlib.h>
#include <stdint.h>

#include "Oscillator.h"


void OscillatorBlockTillReady(void);

void OscillatorBlockTillReady(void)
{

}

void oscillatorInit(uint8_t systemClockSelect, uint8_t internalOscFrequency, uint8_t pllEnable)
{
	//Oscon
	OSCCONbits.SPLLEN = pllEnable;
	OSCCONbits.SCS = systemClockSelect;
	if(systemClockSelect == OSCILLATOR_SELECT_INTERNAL)
		OSCCONbits.IRCF = internalOscFrequency;

}
