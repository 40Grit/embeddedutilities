#include <pic.h>
#include <stdint.h>
#include <stdlib.h>

uint8_t readRB0(void)
{
    return PORTBbits.RB0;
}

uint8_t readRB1(void)
{
    return PORTBbits.RB1;
}

uint8_t readRB2(void)
{
    return PORTBbits.RB2;
}

uint8_t readRB3(void)
{
    return PORTBbits.RB3;
}