#ifndef TIMER1_H
#define TIMER1_H

void timer1Init(void);
void timer1Reset(void);
uint8_t timer1IsEnabled(void);

#endif /*TIMER1_H*/