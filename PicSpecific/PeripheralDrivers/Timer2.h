#ifndef TIMER2_H
#define TIMER2_H

int8_t timer2RequestEnable(uint8_t enable);

int8_t timer2Init(uint8_t setPoint, uint8_t prescaler, uint8_t postscaler);
int8_t timer2IsEnabled(void);
#endif /*TIMER2_H*/