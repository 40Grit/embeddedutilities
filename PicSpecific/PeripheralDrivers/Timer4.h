#ifndef TIMER4_H
#define TIMER4_H

int8_t timer4RequestDisable(void);
int8_t timer4RequestEnable(void);
int8_t timer4Init(uint8_t setPoint, uint8_t prescaler, uint8_t postscaler);
int8_t timer4IsEnabled(void);

#endif