///****************************************************************************
//  Function:
//    int16_t get_adc(uint8_t channel)
//
//  Summary:
//    Initializes the inputs on the F1 Evaluation platform
//
//  Description:
//    This function performs a single ADC conversion blocking until it is finished.
//    The result is returned as a signed 16-bit integer.
//    This function blocks until the ADC is finished.
//
//  Precondition:
//    None
//
//  Parameters:
//    uint8_t channel : the channel number to convert
//
//  Returns:
//    int16_t of the current ADC value
//
//  Remarks:
//    The ADC is turned on and off in this function.
//    The channel is left at the last converted channel.
//    This function waits ~6 us ( ~50 instruction cycles @ Fosc = 32 MHz)
//    for acquisition when channel change is detected.
// ***************************************************************************/
//int16_t adcConvert(uint8_t channel)
//{
//    char x;
//    ADON = 1; // ADC is ON
//    ADNREF = 0; // Negative Reference = VSS
//    ADPREF0 = 0; // Positive Reference = Vdd
//    ADPREF1 = 0;
//    ADFM = 1; // right justify
//
//    if (ADCON0bits.CHS != channel)
//    {
//	ADCON0bits.CHS = channel; // select ADC channel
//	for (x = 8; x; x--); // wait acquisition time (6 Tcy in loop).
//    }
//
//    GO_nDONE = 1; // Start the conversion
//
//    for (x = 200; x; x--) // ADC watchdog loop
//    {
//	if (GO_nDONE == 0) break; // exit loop on conversion finished
//    }
//
//    // wait for conversion
//    ADON = 0; // turn ADC off to save a little power
//    return (ADRESH << 8 | ADRESL); // return the result
//}
