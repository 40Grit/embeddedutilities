#ifndef BLINK_H
#define BLINK_H
#include <stdlib.h>
#include <stdint.h>

int8_t blinkStart(void (*functionToBlink)(uint8_t));
int8_t blinkStop(void (*functionToStopBlink)(uint8_t), uint8_t stopStateFlag);

#endif /*BLINK_H*/