/* 
 * File:   MainTest.h
 * Author: Grit
 *
 * Created on February 14, 2014, 6:52 PM
 */

#ifndef MAINTEST_H
#define	MAINTEST_H

#ifdef	__cplusplus
extern "C" {
#endif

#define TIMER_TEST 0    //Test Timer Identifier

#define BUTTON_TEST 0    //Test Button identifier

#ifdef	__cplusplus
}
#endif

#endif	/* MAINTEST_H */

