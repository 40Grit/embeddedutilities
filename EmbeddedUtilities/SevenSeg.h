
#ifndef SEVENSEG_H
#define SEVENSEG_H

#include <stdlib.h>
#include <stdint.h>


#define SEG_A   0x01
#define SEG_B   0x02
#define SEG_C   0x04
#define SEG_D   0x08
#define SEG_E   0x10
#define SEG_F   0x20
#define SEG_G   0x40
#define SEG_DP  0x80

#define DIGIT_0 (SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | NULL ) //x011-1111 -> 0-> 0x3F
#define DIGIT_1 (NULL  | SEG_B | SEG_C | NULL  | NULL  | NULL  | NULL ) //x000-0110 -> 1-> 0x06
#define DIGIT_2 (SEG_A | SEG_B | NULL  | SEG_D | SEG_E | NULL  | SEG_G) //x101-1011 -> 2-> 0x5B
#define DIGIT_3 (SEG_A | SEG_B | SEG_C | SEG_D | NULL  | NULL  | SEG_G) //x100-1111 -> 3-> 0x4F
#define DIGIT_4 (NULL  | SEG_B | SEG_C | NULL  | NULL  | SEG_F | SEG_G) //x110-0110 -> 4-> 0x66
#define DIGIT_5 (SEG_A | NULL  | SEG_C | SEG_D | NULL  | SEG_F | SEG_G) //x110-1101 -> 5-> 0x6D
#define DIGIT_6 (SEG_A | NULL  | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G) //x111-1101 -> 6-> 0x7D
#define DIGIT_7 (SEG_A | SEG_B | SEG_C | NULL  | NULL  | NULL  | NULL ) //x000-0111 -> 7-> 0x07
#define DIGIT_8 (SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G) //x111-1111 -> 8-> 0x7F
#define DIGIT_9 (SEG_A | SEG_B | SEG_C | NULL  | NULL  | SEG_F | SEG_G) //x110-0111 -> 9-> 0x67


#define DIGIT_A (SEG_A | SEG_B | SEG_C         | SEG_E | SEG_F | SEG_G) //0
#define DIGIT_B (                SEG_C | SEG_D | SEG_E | SEG_F | SEG_G) //0
#define DIGIT_C (SEG_A |                 SEG_D | SEG_E | SEG_F |      ) //0
#define DIGIT_D (        SEG_B | SEG_C | SEG_D | SEG_E |         SEG_G) //0
#define DIGIT_E (SEG_A |                 SEG_D | SEG_E | SEG_F | SEG_G) //0
#define DIGIT_F (SEG_A |                       | SEG_E | SEG_F | SEG_G) //0

#define LETTER_A (SEG_A | SEG_B | SEG_C | SEG_E | SEG_F | SEG_G)
#define LETTER_C (SEG_A | SEG_D | SEG_E | SEG_F)
#define LETTER_E (SEG_A | SEG_D | SEG_E | SEG_F | SEG_G)
#define LETTER_F (SEG_A | SEG_E | SEG_F | SEG_G)
#define LETTER_H (SEG_B | SEG_C | SEG_E | SEG_F | SEG_G)
#define LETTER_I (SEG_E | SEG_F)
#define LETTER_L (SEG_D | SEG_E | SEG_F)
#define LETTER_P (SEG_A | SEG_B | SEG_E | SEG_F | SEG_G)
#define LETTER_S (SEG_A | SEG_F | SEG_G | SEG_C | SEG_D)
#define LETTER_U (SEG_B | SEG_C | SEG_D | SEG_E | SEG_F)
#define LETTER_b (SEG_C | SEG_D | SEG_E | SEG_F | SEG_G)
#define LETTER_c (SEG_D | SEG_E | SEG_G)
#define LETTER_d (SEG_B | SEG_C | SEG_D | SEG_E | SEG_G)
#define LETTER_h (SEG_C | SEG_E | SEG_F | SEG_G)
#define LETTER_t (SEG_D | SEG_E | SEG_F | SEG_G)
#define LETTER_r (SEG_E | SEG_G)
#define LETTER_n (SEG_C | SEG_E | SEG_G)


typedef struct
{
    void (*const setSegment[7])(uint8_t enable);
    uint8_t segmentData;
}SevenSegmentDigit;


void sevenSegDisplayTime(uint8_t amPm, uint24_t time);
void sevenSegDisplayTimeAmPm(uint8_t hours, uint8_t minutes, uint8_t seconds, uint8_t amPm);
void sevenSegDisplayNumberPair(uint8_t digitPairId, uint8_t number);
void sevenSegDisplayNumber(uint24_t number);
void sevenSegDisplayDigit(uint8_t digitId, uint8_t numeral);
void sevenSegClear(void);



#endif /*SevenSeg.h*/
