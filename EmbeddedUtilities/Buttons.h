//Grit

#ifndef BUTTONS_H
#define BUTTONS_H
#include <stdlib.h>
#include <stdint.h>

void buttonsOnChangeHandler(void);
void buttonsDebounceTimerHandler(void);
void buttonsMain(void);

void buttonsInitModule(int8_t (*debounceTimerEnable)(uint8_t), void (*onChangeInterruptEnable)(uint8_t));

void buttonSetup(uint8_t buttonIndex, uint8_t(*readPort)(void), uint8_t debounceThreshold, uint16_t heldThreshold,
                 void(*onPressed)(void) , void(*onHeld)(void) , void(*onReleased)(void),
				 void(*onPressedAndReleased)(void) , void(*onHeldAndReleased)(void));

void buttonAdjust(uint8_t buttonIndex, uint16_t heldThreshold,
		  void(*onPressed)(void) , void(*onHeld)(void) , void(*onReleased)(void),
		  void(*onPressedAndReleased)(void) , void(*onHeldAndReleased)(void));

#endif /*BUTTONS_H*/

