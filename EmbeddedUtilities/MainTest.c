/* 
 * File:   MainTest.c
 * Author: Grit
 *
 * Created on February 14, 2014, 6:30 PM
 */

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <xc.h>
#include "Buttons.h"
#include "RealTimeClock.h"
#include "SoftTimer.h"
#include "SoftAlarm.h"

#include "Timer1.h"
#include "Timer2.h"
#include "Interrupt.h"
#include "Oscillator.h"
#include "IoPorts.h"


static void mainInit(void);
static void buttonPressed(void);
static void buttonHeld(void);
static void buttonReleased(void);
static void buttonPressedAndReleased(void);
static void buttonHeldAndReleased(void);

#define TIMER_0  0
#define BUTTON_0 0

static void doThisEvery10Seconds(void);
int main(void)
{
	interruptGlobalEnable(false);
	interruptPeripheralEnable(false);

    mainInit();

	//Initialize real time clock providing the module with timer1 driver functions
    //timer one provides 1 second interval using external crystal
	rtcInit(timer1Init, timer1Reset);

	//initialize softTimer and softAlarm modules
	softTimerInit();
	softAlarmInit();

    //Initialize interrupt on change for button module
	interruptOnChangeInit();

	//Initialize timer2 as 1ms timer in preparation for button module
    timer2Init(125, 16, 1);//TODO:add timer2 hardware documentation to driver code pr2, prescaler, postscaler etc.

	//initialize button module, providing necessary timer and interrupt on change driver calls
    buttonsInitModule(timer2RequestEnable, interruptOnChangeEnable);
	buttonSetup(BUTTON_0,readRB0,20,2000,buttonPressed,buttonHeld,buttonReleased,buttonPressedAndReleased,buttonHeldAndReleased);
	
	interruptPeripheralEnable(true);
	interruptGlobalEnable(true);
	
	//Set timer to call doThisEvery10Seconds repeatedly every 10 seconds
	softTimerSet(TIMER_0, 10, doThisEvery10Seconds, true);

    while(1)
    {
		softTimerMain(); //Check soft timers
		softAlarmMain(); //Check soft alarms
        buttonsMain();   //Check buttons

		//No sleeping on the job
		if(!timer2IsEnabled()) SLEEP();
	}
	return 0;
}

void interrupt isr(void)
{
    if (TMR1IF)                             //If timer 1 is flagged
    {
        rtcHandler();
		softTimerHandler();
		softAlarmHandler();
        TMR1IF = 0;
    }
    if (TMR2IF)                             //Check timer 2
    {
        buttonsDebounceTimerHandler();
        TMR2IF = 0;
    }
    if (IOCIF)                              //check for port b changes
    {
        IOCBF &= 0;
        buttonsOnChangeHandler();
        IOCIF = 0;
    }
}

static void buttonPressed(void){/*do something*/}
static void buttonHeld(void){/*do something*/}
static void buttonReleased(void){/*do something*/}
static void buttonPressedAndReleased(void){/*do something*/}
static void buttonHeldAndReleased(void){/*do something*/}

static void doThisEvery10Seconds(void){/*do something*/}

static void mainInit(void)
{
    // set all ports tristate
    TRISA = 0xFF;
    TRISB = 0xFF;
    TRISC = 0xFF;

    PORTB = 0x00;
    TRISB = 0xFF;

	oscillatorInit(OSCILLATOR_SELECT_INTERNAL,OSCILLATOR_FREQUENCY_HIGH_8MHZ, 0);
}