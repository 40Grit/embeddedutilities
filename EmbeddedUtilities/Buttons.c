/**
 * Grit
 *
 *Button.c
 * Module to debounce and respond to multiple hardware button presses, holds, and releases
 * Uses an "interrupt on chage" peripheral to respond to a change in button pin status
 * Upon detecting a change a timer is activated to continue sampling the button's pin
 * If the port is sampled high for the length of the given debounce threshold the button
 *  is considered pressed, and the given pressedCallback is callled
 * The timer continues operation to detect if the button is being held based on the held
 *  threshold. If the pin is sampled high for the length of the heldThreshold the heldCallback
 *  is called
 * Upon release of the button after being pressed releasedCallback is called
 *  and the timer resource is returned
 */

/*TODO:Adress active high/low ness*/
/* TODO: attempt to create a "static" structure for the module that contains functions to operate on a Button object*/
/* TODO: 2/14/2014 Force user to declare their own buttons so they don't have to dig around in here to change BUTTON_COUNT etc.*/
/* TODO: 2/14/2014 Update java doc and comments*/

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <pic.h>

#include "Buttons.h"


#define BUTTON_COUNT 1

typedef struct
{
	uint8_t buttonEnabled:1;			//Flag to indicate that button should be serviced
	uint8_t pressedFlag:1;				//Indicates button was pressed
    uint8_t heldFlag:1;					//Indicates button just pass the held threshold
    uint8_t releasedFlag:1;				//Indicates button was released
	uint8_t pressedAndReleasedFlag:1;	//Indicates button was released after being pressed and wasn't held
	uint8_t heldAndReleasedFlag:1;		//Indicates button was released after being held
	uint8_t wasPressedFlag:1;			//incase buttons parameters are changed before button was released
	uint8_t wasHeldFlag:1;				//Indicates button had been held upon release
	uint8_t wasReleasedFlag:1;			//Indicates that button was released, used to prevent a press if button hasn't been released

	uint8_t debounceThreshold;			//Maximum length of bounce time
	uint16_t debounceCount;				//Number of samples for which button has not bounced
    uint16_t heldThreshold;				//Length of time for button to be considered held
	uint8_t (* readPort)(void);    //function to read buttons pin status
    void (*pressedCallback)(void);		//Called when button is pressed
    void (*heldCallback)(void);			//Called when button is held
    void (*releasedCallback)(void);		//Called when button is released
	void (*pressedAndReleasedCallback)(void);
	void (*heldAndReleasedCallback)(void);
}Button;

static int8_t (*timerEnable)(uint8_t)	= NULL;
static void (*onChangeEnable)(uint8_t)	= NULL;

static Button buttonList[BUTTON_COUNT] = {0};//TODO: make user decalre this sort of stuff on their own

static uint8_t debounceActive = false;//TODO: re-assess which flags are actually necessarry




/**
 * buttonInitModule:
 * Provide module with functions to request/return on change interrupt and timer resources
 * Initializes button list
 * Call before buttonSetup()
 *
 * @param debounceTimerEnable   function that will turn on the timer used to sample pin status
 * @param debounceTimerOff      function that will turn off the timer used to sample pin status
 * @param onChangeInterruptOn   function that will turn on an "on change interrupt" peripheral
 * @param onChangeInterruptOff  function that will turn off an "on change interrupt" peripheral
 */
void buttonsInitModule(int8_t (*debounceTimerEnable)(uint8_t), void (*onChangeInterruptEnable)(uint8_t))
{
    uint8_t index;

	timerEnable = debounceTimerEnable;
	onChangeEnable = onChangeInterruptEnable;

    for(index = 0; index < BUTTON_COUNT; index++)
    {
        buttonList[index].buttonEnabled = false;
    }
}


/**
 * buttonSetup:
 * Initializes and enables a 'button'
 * Call after buttonInitModule()
 * @param buttonIndex
 * @param debounceThreshold
 * @param heldThreshold
 * @param readPort
 * @param onPressed
 * @param onHeld
 * @param onReleased
 */
void buttonSetup(uint8_t buttonIndex, uint8_t(*readPort)(void), uint8_t debounceThreshold, uint16_t heldThreshold,
                 void(*onPressed)(void) , void(*onHeld)(void) , void(*onReleased)(void),
				 void(*onPressedAndReleased)(void) , void(*onHeldAndReleased)(void))
{
	buttonList[buttonIndex].buttonEnabled = false;

	buttonList[buttonIndex].readPort			= readPort;
    buttonList[buttonIndex].debounceThreshold	= debounceThreshold;
    buttonList[buttonIndex].heldThreshold		= heldThreshold;

    buttonList[buttonIndex].pressedCallback				= onPressed;
    buttonList[buttonIndex].heldCallback					= onHeld;
    buttonList[buttonIndex].releasedCallback				= onReleased;
	buttonList[buttonIndex].pressedAndReleasedCallback	= onPressedAndReleased;
	buttonList[buttonIndex].heldAndReleasedCallback		= onHeldAndReleased;

	buttonList[buttonIndex].wasReleasedFlag	= true;
    
	buttonList[buttonIndex].buttonEnabled	= true;
	onChangeEnable(true);
}

void buttonAdjust(uint8_t buttonIndex, uint16_t heldThreshold, 
				  void(*onPressed)(void) , void(*onHeld)(void) , void(*onReleased)(void),
				  void(*onPressedAndReleased)(void) , void(*onHeldAndReleased)(void))
{
    //Early return if button is not enabled
    if(!(buttonList[buttonIndex].buttonEnabled))
        return;

    buttonList[buttonIndex].buttonEnabled = 0;          //Prevent flagging during adjustment

    buttonList[buttonIndex].heldThreshold = heldThreshold;

    buttonList[buttonIndex].pressedCallback			= onPressed;
    buttonList[buttonIndex].heldCallback				= onHeld;
    buttonList[buttonIndex].releasedCallback			= onReleased;
	buttonList[buttonIndex].pressedAndReleasedCallback= onPressedAndReleased;
	buttonList[buttonIndex].heldAndReleasedCallback	= onHeldAndReleased;

	buttonList[buttonIndex].debounceCount			= 0;
	buttonList[buttonIndex].heldFlag				= 0;
	buttonList[buttonIndex].pressedFlag				= 0;
	buttonList[buttonIndex].releasedFlag			= 0;
	buttonList[buttonIndex].pressedAndReleasedFlag	= 0;
	buttonList[buttonIndex].heldAndReleasedFlag		= 0;

    buttonList[buttonIndex].buttonEnabled	= 1;

}

/**
 * buttonsCheckIn:
 * Checks the flags of each button and calls appropriate callbacks
 * PLACE IN MAIN LOOP
 */
void buttonsMain(void)
{
    uint8_t buttonIndex = 0;

    //Check each buttons flags, call appropriate listeners
    //Clear appropriate flags
    for (buttonIndex = 0; buttonIndex < BUTTON_COUNT; buttonIndex++)
    {
		if(!buttonList[buttonIndex].buttonEnabled) continue;
        if (buttonList[buttonIndex].pressedFlag)
        {
            buttonList[buttonIndex].pressedFlag = false;
            if (buttonList[buttonIndex].pressedCallback) buttonList[buttonIndex].pressedCallback();
        }
        if (buttonList[buttonIndex].heldFlag)
        {
            buttonList[buttonIndex].heldFlag = false;
            if (buttonList[buttonIndex].heldCallback) buttonList[buttonIndex].heldCallback();
        }
        if (buttonList[buttonIndex].releasedFlag)
        {
            buttonList[buttonIndex].releasedFlag = false;
            if (buttonList[buttonIndex].releasedCallback) buttonList[buttonIndex].releasedCallback();
        }
		if (buttonList[buttonIndex].pressedAndReleasedFlag)
        {
            buttonList[buttonIndex].pressedAndReleasedFlag = false;
            if (buttonList[buttonIndex].pressedAndReleasedCallback) buttonList[buttonIndex].pressedAndReleasedCallback();
        }
		if (buttonList[buttonIndex].heldAndReleasedFlag)
        {
            buttonList[buttonIndex].heldAndReleasedFlag = false;
            if (buttonList[buttonIndex].heldAndReleasedCallback) buttonList[buttonIndex].heldAndReleasedCallback();
        }
    }
	return;
}

/**
 * debounceStartHandler:
 * Handles an on change interrupt, starts the debounce timer and returns
 * the onChange interrupt resource to prevent redundant interruptions
 * PLACE IN ISR
 */

void buttonsOnChangeHandler(void)
{
		timerEnable(true); //Request a timer to sample port
    onChangeEnable(false); //Receiving on change notices would be redundant while sampling
    debounceActive = true;
}

/**
 * debounceTimerHandler
 * Checks the ports of each button and increments their bounceCounter if active
 * Further checks if thresholds have been met and sets flags appropriately
 * PLACE IN ISR
 */
void buttonsDebounceTimerHandler(void)
{
    //FIXME: doneDebouncingFlag feels redundant
    uint8_t doneDebouncingFlag = true;
    uint8_t buttonIndex;
    Button* button;
    if(!debounceActive) return;

    for (buttonIndex = 0; buttonIndex < BUTTON_COUNT; buttonIndex++)
    {
        button = &buttonList[buttonIndex];
		if(!button->buttonEnabled) continue;

        if (button->readPort())
        {
            doneDebouncingFlag = false;
			//If the debounce count is less than the threshold increment bounce count and check threshold
            if (button->debounceCount < button->debounceThreshold)
            {
                button->debounceCount++;
                if ((button->debounceCount >= button->debounceThreshold) && button->wasReleasedFlag)
                {
                    button->pressedFlag = true;
					button->wasPressedFlag = true;
					button->wasReleasedFlag = false; //button was pressed must be released to be pressed again
                }
            }
            else if (button->debounceCount < button->heldThreshold && button->wasPressedFlag)
            {
                button->debounceCount++;
                if (button->debounceCount >= button->heldThreshold)
                {
					button->heldFlag = true;
					button->wasPressedFlag	= false;
					button->wasHeldFlag		= true;
                }
            }
        }
		else 
        {//Port is not active, set flags acording to whether button was pressed or held before inactive
			button->debounceCount = false; //reset debounce count since button was released or is still bouncing

			if (button->wasPressedFlag)
            {//button was pressed before release flag accordingly
                button->releasedFlag			= true;
				button->pressedAndReleasedFlag  = true;
				
				button->wasReleasedFlag = true;	//Indicate for next press that button was released
				button->wasPressedFlag	= false;
            }
			else if (button->wasHeldFlag)
            {//button was held before release flag accordingly
                button->releasedFlag		 = true;
				button->heldAndReleasedFlag  = true;

				button->wasReleasedFlag = true;	//Indicate for next press that button was released
				button->wasHeldFlag		= false;
            }
        }
    }

    //If ports are all low we stop sampling and await the next port transition
    if (doneDebouncingFlag)
    {
        debounceActive = false;
		timerEnable(false);
        onChangeEnable(true);
    }
}
