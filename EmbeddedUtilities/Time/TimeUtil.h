
#ifndef TIMEUTIL_H
#define	TIMEUTIL_H

#include <stdlib.h>
#include <stdint.h>

void timeUtilToTime(uint24_t inputSeconds, uint8_t* hours, uint8_t* minutes, uint8_t* seconds);
uint24_t timeUtilToSeconds(uint8_t hours, uint8_t minutes, uint8_t seconds);



#endif	/* TIMEUTIL_H */

