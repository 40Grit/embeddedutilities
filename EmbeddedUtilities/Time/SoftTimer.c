#include<stdlib.h>
#include<stdint.h>

#include "TimeUtil.h"
#include "RealTimeClock.h"
#include "SoftTimer.h"

#define TIMER_COUNT 1

typedef struct
{
	uint8_t flag:1;
	uint8_t expiredFlag:1;
	uint8_t pausedFlag:1;
	uint8_t repeatFlag:1;
	uint8_t incallbackFlag:1;
	uint24_t setpoint;
	uint24_t counter;
	void (*callback)(void);
} SoftTimer;


void softTimerMain(void);
void softTimerInit(void);

static SoftTimer timerList[TIMER_COUNT] = {0}; //List of timers

static uint8_t enableTimersFlag = 1;


//----------------TIMER FUNCTIONS------------------------------------------------------//

/**rtcAddTimer: Sets a timer for the given number of hours, minutes, and seconds
 * Executes a provided callback function when the timer is up
 * Returns the index of set timer, which can be used with rtcGetTimerCount()
 *@param hours   The number of hours to count no more than 24
 *@param minutes    The number of minutes to count no more than 60
 *@param seconds    The number of seconds to count no more than 60
 *@param callback     A function pointer to call back when the timer is up
 */
int8_t softTimerSet(uint8_t timerId, uint24_t seconds, void (*callback)(void), uint8_t repeat)
{
	//Reject bogus input
	if (timerId >= TIMER_COUNT)
		return -1;


	//!!!!!!!!!!Dissalow setting of the timer if this has been called from within it's callback
	if (timerList[timerId].incallbackFlag)
		return -2;

	timerList[timerId].setpoint = seconds;
	timerList[timerId].counter = seconds;

	timerList[timerId].callback = callback;
	timerList[timerId].flag = 0;
	timerList[timerId].expiredFlag = 0;
	timerList[timerId].repeatFlag = repeat;

	return 0;
}

void softTimerAdjust(uint8_t timerId, uint24_t seconds)
{
	timerList[timerId].setpoint = seconds;
}

void softTimerPause(uint8_t timerId)
{
	timerList[timerId].pausedFlag = 1;
}

void softTimerRestart(uint8_t timerId)
{
	timerList[timerId].pausedFlag = 0;
}

/**rtcCancelTimer: cancels timer, not equivalent to pause
 *
 * @param timerId:  timer to cancel
 */
void softTimerCancel(uint8_t timerId)
{
	timerList[timerId].repeatFlag = 0;
	timerList[timerId].expiredFlag = 1;
	timerList[timerId].flag = 0; //Needed???
}

uint8_t softTimerIsEnabled(uint8_t timerId)
{
	return (uint8_t)!(timerList[timerId].expiredFlag);
}

/**initTimerList():
 *Initializes each timer
 */
void softTimerInit(void)
{
	uint8_t count = 0;
	for (count = 0; count < TIMER_COUNT; count++)
	{
		timerList[count].expiredFlag = 1;
		timerList[count].flag = 0;
		timerList[count].pausedFlag = 0;
		timerList[count].callback = 0;
		timerList[count].repeatFlag = 0;
		timerList[count].incallbackFlag = 0;
	}
}

uint24_t softTimerGetCount(uint8_t timerId)
{
	return timerList[timerId].counter;
}


/*
 * Checks each timers flags and executes callback accordingly
 * NOTE: should probably just be in rtcMain
 */
 void softTimerMain(void)
{
	uint8_t timerIndex  = 0;
	for (timerIndex = 0; timerIndex < TIMER_COUNT; timerIndex++)
	{
		if(timerList[timerIndex].expiredFlag) continue;
		if (timerList[timerIndex].flag)
		{
			//Execute callback
			timerList[timerIndex].incallbackFlag = 1;
			timerList[timerIndex].callback();
			timerList[timerIndex].incallbackFlag = 0;

			//Clean Up, reload timer if repeat is enabled
			if (timerList[timerIndex].repeatFlag)
			{
				timerList[timerIndex].counter = timerList[timerIndex].setpoint;
			}
			else
			{
				timerList[timerIndex].expiredFlag = 1;
			}
			//Clear flag after reloading
			timerList[timerIndex].flag = 0;
		}
	}
}


void softTimerCancelAll(void)
{
	enableTimersFlag = 0;
	softTimerInit();
	enableTimersFlag = 1;
}

void softTimerHandler(void)
{
	uint8_t timerIndex;
	if (enableTimersFlag)
	{
		for (timerIndex = 0; timerIndex < TIMER_COUNT; timerIndex++)
		{
			//If this timer is expired move to the next timer
			if (timerList[timerIndex].expiredFlag || timerList[timerIndex].pausedFlag)
				continue;

			timerList[timerIndex].counter--;
			//Else check if timer has ended and flag
			if (!timerList[timerIndex].counter)
			{
				timerList[timerIndex].flag = 1;
			}
		}
	}
}
