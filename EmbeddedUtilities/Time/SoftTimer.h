#ifndef SOFTTIMER_H
#define SOFTTIMER_H

void softTimerInit(void);
void softTimerMain(void);
int8_t softTimerSet(uint8_t timerId, uint24_t seconds, void(*callBack)(void), uint8_t repeatFlag);
void softTimerAdjust(uint8_t timerId, uint24_t seconds);
void softTimerPause(uint8_t timerId);
void softTimerRestart(uint8_t timerId);
int8_t softTimerGetCountInTime(uint8_t index, uint8_t* hours, uint8_t* minutes, uint8_t* seconds);
uint24_t softTimerGetCount(uint8_t timerId);
void softTimerCancel(uint8_t timerId);
uint8_t softTimerIsEnabled(uint8_t timerId);
void softTimerCancelAll(void);

void softTimerHandler(void);

#endif /*SOFTTIMER_H*/