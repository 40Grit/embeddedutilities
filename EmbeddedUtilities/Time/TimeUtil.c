#include <stdint.h>
#include <stdlib.h>

/**rtcToSeconds:
 * Converts unit time into seconds
 * @param hours
 * @param minutes
 * @param seconds
 * @return: time in seconds
 */
uint24_t timeUtilToSeconds(uint8_t hours, uint8_t minutes, uint8_t seconds)
{
	uint24_t timeInSeconds = 0;
	timeInSeconds  = ((uint24_t)hours) * 3600;
	timeInSeconds += (uint24_t)(minutes * 60);
	timeInSeconds += seconds;
	return timeInSeconds;
}

/**rtcToTime:
 * Converts seconds into unit time
 * @param inputSeconds: seconds to be converted
 * @param hours:   output hours
 * @param minutes: output minutes
 * @param seconds: output seconds
 * @return: blah
 */
void timeUtilToTime(uint24_t inputSeconds, uint8_t* hours, uint8_t* minutes, uint8_t* seconds)
{
	uint24_t leftoverSeconds = 0;

	*hours = (uint8_t)(inputSeconds / 3600);
	leftoverSeconds = inputSeconds % 3600;
	*minutes = (uint8_t)(leftoverSeconds / 60);
	*seconds = (uint8_t)(leftoverSeconds % 60);
}

