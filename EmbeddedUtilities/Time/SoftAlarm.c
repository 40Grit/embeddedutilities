//Grit

#include<stdlib.h>
#include<stdint.h>

#include "TimeUtil.h"
#include "RealTimeClock.h"



#define ALARM_COUNT 1


typedef struct
{
	uint8_t flag:1;
	uint8_t expiredFlag:1;
	uint24_t alarmTime;
	void (*callback)(void);
} AlarmEvent;


void softAlarmMain(void);

void softAlarmInit(void);

static AlarmEvent alarmList[ALARM_COUNT] = {0}; //List of alarms

void softAlarmHandler(void);

static uint8_t enableAlarmsFlag = 1;


//------ALARM FUNCTIONS-------------------------------------------------/
/**rtcAddAlarm:Sets and alrm for the time specified by hour, minute, second
 * Executes the provided callback function when the time is reached
 * Returns error when provided time is invalid or all alarms are in use
 *@param hours   The number of hours to count no more than 24
 *@param minutes    The number of minutes to count no more than 60
 *@param seconds    The number of seconds to count no more than 60
 *@param callback     A function pointer to call back when the timer is up
 */
int8_t softAlarmSet(uint8_t alarmId, uint24_t timeInSeconds, void (*callback)(void))
{
	//FIXME: Set bounds
	if (alarmId >= ALARM_COUNT)
		return -1;


	alarmList[alarmId].callback = callback;
	alarmList[alarmId].flag = 0;
	alarmList[alarmId].expiredFlag = 0;
	alarmList[alarmId].alarmTime = timeInSeconds;

	return alarmId;
}

/**initAlarmList():
 *Initializes each alarm
 */
void softAlarmInit(void)
{
	uint8_t count = 0;
	for (count = 0; count < ALARM_COUNT; count++)
	{
		alarmList[count].expiredFlag = 1;
		alarmList[count].callback = 0;
	}
}

void softAlarmGetTime(uint8_t alarmId, uint8_t* hours, uint8_t* minutes, uint8_t* seconds)
{
	timeUtilToTime(alarmList[alarmId].alarmTime, hours, minutes, seconds);
}

uint24_t softAlarmGetTimeInSeconds(uint8_t alarmId)
{
	return alarmList[alarmId].alarmTime;
}


/*
 * Checks each alarms flags and executes callback accordingly
 * NOTE: should probably just be in rtcMain
 */
void softAlarmMain(void)
{
	uint8_t alarmIndex;

	for (alarmIndex = 0; alarmIndex < ALARM_COUNT; alarmIndex++)
	{
		if (alarmList[alarmIndex].flag)
		{
			alarmList[alarmIndex].callback();
			alarmList[alarmIndex].flag = 0;
		}
	}
}

void softAlarmHandler(void)
{
	uint8_t alarmIndex = 0;

	if (enableAlarmsFlag)
	{
		for (alarmIndex = 0; alarmIndex < ALARM_COUNT; alarmIndex++)
		{
			//If this alarm is expired continue to the next alarm
			if (alarmList[alarmIndex].expiredFlag)
				continue;

			//Check if it is the alarms time and flag
			if (alarmList[alarmIndex].alarmTime == rtcGetTimeInSeconds())
			{
				alarmList[alarmIndex].flag = 1;
				alarmList[alarmIndex].expiredFlag = 1;
			}
		}
	}
}

