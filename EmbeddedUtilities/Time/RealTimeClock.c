/**RealTimeClock
 * Thingy to manage a realtime clock, set timers, and alarms.
 * Users provide hardware timer control functions through rtcInit().
 * Call rtcHandler() in the hardware timer isr
 * Call rtcMain() in main to check flags and initiate callbacks
 * Set number of alarms and counters using #define TIMER_COUNT and ALARM_COUNT
 * ^TODO: remove this requirement, build into rtcInit
 */

#include <stdint.h>
#include <stdlib.h>
#include <xc.h>
#include "TimeUtil.h"
#include "Timer1.h"

#define SECONDS_IN_A_DAY 86400

static uint24_t time = 0; //Main time variable
static void (* resetHardwareTimer)(void) = NULL;

/**rtcInit():
 * Initializes timer list, alarm list, stashes function to reset hardware timer
 * @param initTimerFunction:    Probably shouldn't ask for this, leave timerInit to user?
 * @param resetTimerFunction:   function to reset timer 1, used by rtcHandler
 */
void rtcInit(void (*initTimerFunction)(void), void (* resetTimerFunction)(void))
{
	resetHardwareTimer = resetTimerFunction; //Will be called in rtcHandler
	initTimerFunction();	//NOTE: Leave this upto user???
	time = 0;
}


//----------------CLOCK TIME FUNCTIONS--------------------------------------------//
/**rtcSetTime: sets the clock
 * TODO: early return on bogus input
 * @param hour
 * @param minute
 * @param second
 */
void rtcSetTime(uint24_t seconds)
{
	time = seconds;
}

/**rtcGetTimer: Places current hour, minute, and second into referenced arguments
 * @param hours
 * @param minutes
 * @param seconds
 * @return
 */
int8_t rtcGetTime(uint8_t* hours, uint8_t* minutes, uint8_t* seconds)
{
	timeUtilToTime(time, hours, minutes, seconds);
	return 0;
}

uint24_t rtcGetTimeInSeconds(void)
{
	return time;
}

/**rtcHandler:
 *Place in hardware timer isr
 *resets hardware timer, increments time
 *flags alarms and timers
 */
void rtcHandler(void)
{
	resetHardwareTimer();
	time++;
	if (time >= SECONDS_IN_A_DAY) time = 0;
}
