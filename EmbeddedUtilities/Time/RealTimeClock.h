//TODO: split timer and alarm into own modules

#ifndef REAL_TIME_CLOCK_H
#define REAL_TIME_CLOCK_H

#include <stdlib.h>
#include <stdint.h>


void rtcInit(void(*initTimerFunction)(void) , void(* resetTimerFunction)(void) );

void rtcMain(void);
void rtcHandler(void);
void rtcSetTime(uint24_t seconds);

uint24_t rtcGetTimeInSeconds(void);
void rtcIncrementHour(void);
void rtcIncrementMinute(void);
void rtcIncrementSecond(void);

#endif /*REAL_TIME_CLOCK_H*/