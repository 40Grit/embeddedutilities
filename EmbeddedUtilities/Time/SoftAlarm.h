/* 
 * File:   SoftAlarm.h
 * Author: Grit
 *
 * Created on February 11, 2014, 7:54 PM
 */

#ifndef SOFTALARM_H
#define	SOFTALARM_H

void softAlarmInit(void);
void softAlarmMain(void);

int8_t softAlarmSet(uint8_t alarmIndex, uint24_t timeInSeconds, void(*callBack)(void) );
void softAlarmGetTime(uint8_t alarmId, uint8_t* hours, uint8_t* minutes, uint8_t* seconds);
uint24_t softAlarmGetTimeInSeconds(uint8_t alarmId);
void rtcCancelAlarms(void);
void softAlarmHandler(void);

#endif	/* SOFTALARM_H */

