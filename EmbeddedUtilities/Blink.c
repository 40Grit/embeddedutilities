#include <stdlib.h>
#include <stdint.h>

#include "Blink.h"
#include "RealTimeClock.h"
#include "SoftTimer.h"
#define MAX_SUBSCRIBERS 3

static void blink(void);

static void (*subscriberList[MAX_SUBSCRIBERS])(uint8_t onOff) = {0};

static uint8_t subscriberCount	= 0;
static uint8_t blinkState		= 0;
static uint8_t keepTimerFlag	= 0;

//TODO: Add blink setup to provide a timer versus defining timer in "Main.h"
#define TIMER_BLINK 4 //Dirty 
static void blink(void)
{
	uint8_t index = 0;
	if(!keepTimerFlag)
	{
		softTimerCancel(TIMER_BLINK);
		return;
	}

	for (index = 0; index != MAX_SUBSCRIBERS; index++)
	{
		if(subscriberList[index] != 0) subscriberList[index](blinkState);
	}

	if(blinkState) blinkState = 0x00;
	else blinkState = 0xFF;
}

int8_t blinkStart(void (*functionToBlink)(uint8_t))
{
	uint8_t index = 0;

	if(subscriberCount >= MAX_SUBSCRIBERS) return -1;

	for(index = 0; index != MAX_SUBSCRIBERS; index++)
	{
		if(subscriberList[index]== 0)
		{
			subscriberList[index] = functionToBlink;

			subscriberCount++;
			blinkState = 1;
			functionToBlink(0);
			break;
		}
	}
	if(subscriberCount == 1)
	{
		keepTimerFlag = 1;
		softTimerSet(TIMER_BLINK, 1, blink, 1);
	}
	return 0;
}


int8_t blinkStop(void (*functionToStopBlink)(uint8_t), uint8_t stopStateFlag)
{
	uint8_t index = 0;
	if(!subscriberCount) return -1;
	for(index = 0; index < MAX_SUBSCRIBERS; index++)
	{
		
		if(subscriberList[index] == functionToStopBlink)
		{
			subscriberCount--;
			if(!(subscriberCount)) keepTimerFlag = 0;
			subscriberList[index](stopStateFlag);
			subscriberList[index] = 0;
		}
	}
	return 0;
}

