//Grit
//1/14/2014 Unfinished
//TODO: Simplify into just Seven Segment digit functionality
//TODO: Build on simplified module

#include <stdint.h>
#include <stdlib.h>
#include "SevenSeg.h"
#include "TimeUtil.h"

#define MAX_DIGITS 6
#define MAX_DIGIT_PAIRS 3
#define MAX_DISPLAYABLE 999999

static void refreshDigit(uint8_t digitId);
static void refreshDigits(void);


static const uint8_t numericTable[10] = {DIGIT_0, DIGIT_1, DIGIT_2, DIGIT_3, DIGIT_4, DIGIT_5, DIGIT_6, DIGIT_7, DIGIT_8, DIGIT_9};

static uint8_t colonList[MAX_DIGIT_PAIRS - 1] = {0};

void sevenSegDisplayDigit(uint8_t digitId, uint8_t numeral)
{
	//Early Return on bogus input
	if((digitId >= MAX_DIGITS) || (numeral > 9)) return;
	digitList[digitId].segmentData = numericTable[numeral];
	refreshDigit(digitId);
}

void sevenSegDisplayNumber(uint24_t number)
{
	uint8_t digitIndex = 0;
	uint24_t mod = 0;

	//Early Return on bogus input
	if(number > MAX_DISPLAYABLE) return;
	
	for(digitIndex = 0; digitIndex  < MAX_DIGITS; digitIndex++)
	{
		mod = number % 10;
		number /= 10;
		digitList[digitIndex].segmentData = numericTable[mod];
	}
	refreshDigits();
}

void sevenSegDisplayNumberPair(uint8_t digitPairId, uint8_t number)
{
	uint8_t digitIndex;
	uint8_t mod = 0;

	if((digitPairId > MAX_DIGITS/2) || (number >= 100)) return; //early return on bogus input

	digitIndex = digitPairId * 2u + 1u;
	digitList[digitIndex--].segmentData = number / 10u;
	digitList[digitIndex].segmentData   = number % 10u;

}


void sevenSegDisplayTime(uint8_t amPm, uint24_t time)
{
	uint8_t hours, minutes, seconds;
	timeUtilToTime(time, &hours, &minutes, &seconds);
	sevenSegDisplayTimeAmPm(hours, minutes, seconds, amPm);
}

void sevenSegDisplayTimeAmPm(uint8_t hours, uint8_t minutes, uint8_t seconds, uint8_t amPm)
{
	if((hours > 24) || (minutes > 60) || (seconds > 60)) return; //Early return on bogus input

	if(seconds > 60)
	{
		colonList[1] = 0;
		sevenSegDisplayNumberPair(1u, hours);
		colonList[0] = 1;
		sevenSegDisplayNumberPair(0, minutes);
	}
	else
	{
		sevenSegDisplayNumberPair(2u, hours);
		colonList[1] = 1;
		sevenSegDisplayNumberPair(1u, minutes);
		colonList[0] = 1;
		sevenSegDisplayNumberPair(0u, seconds);
	}

}


static void refreshDigit(uint8_t digitId)
{
	uint8_t segmentMask = 1;
	uint8_t segmentIndex;
	uint8_t segmentData;
	uint8_t maskedData;
		for(segmentIndex = 0; segmentIndex < 7; segmentIndex++)
		{
			segmentData = digitList[digitId].segmentData;
			maskedData = (uint8_t)(segmentData & segmentMask);
			digitList[digitId].setSegment[segmentIndex](maskedData);
			segmentMask <<= 1u;
		}
}

static void refreshDigits(void)
{
	uint8_t digitIndex;
	for(digitIndex = 0; digitIndex < MAX_DIGITS; digitIndex++)
	{
		refreshDigit(digitIndex);
	}
}

void sevenSegClear(void)
{
	uint8_t digitIndex;

	for(digitIndex = 0; digitIndex < MAX_DIGITS; digitIndex++)
	{
		digitList[digitIndex].segmentData = 0;
	}
	refreshDigits();
}